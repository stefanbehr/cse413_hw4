#lang racket

;; Stefan Behr
;; CSE 413
;; Homework 4
;; 10/25/2012

;; Part II. Streams & Things

;; 1. Election year special

(define red-blue (letrec ([red (lambda () (cons "red" blue))]
                          [blue (lambda () (cons "blue" red))])
                   red))

;; tests for red-blue
(define test1
  (car (red-blue)))

(define test2
  (car
   ((cdr (red-blue)))))

(define test3
  (car
   ((cdr
     ((cdr (red-blue)))))))


;; 2. Write a function (take st n)...

(define take (lambda (st n)
               (if (= n 0)
                   '()
                   (cons (car (st)) (take (cdr (st)) (- n 1))))))

;; natural number stream for testing
(define naturals (letrec ([f (lambda (x)
                               (cons x (lambda () (f (+ x 1)))))])
                   (lambda () (f 1))))

;; test inputs for take
(define test4 (list red-blue 3))
(define test5 (list red-blue 4))
(define test6 (list naturals 5))
(define test7 (list naturals 6))

;; 3. Recall from assignment 1...

(define combm (letrec ([factm (letrec ([f-memo null]
                                       [fact (lambda (n)
                                               (let ([f-ans (assoc n f-memo)])
                                                 (if f-ans
                                                     (cdr f-ans)
                                                     (let ([f-result (if (= n 0)
                                                                         1
                                                                         (* n (fact (- n 1))))])
                                                       (begin
                                                         (set! f-memo (cons (cons n f-result) f-memo))
                                                         f-result)))))])
                                fact)]
                       [c-memo null]
                       [comb (lambda (n k)
                               (let ([c-ans (assoc (cons n k) c-memo)])
                                 (if c-ans
                                     (cdr c-ans)
                                     (let ([c-result (cond [(not (and (<= k n)
                                                                      (>= k 0))) "invalid arguments"]
                                                           [else (/ (factm n)
                                                                    (* (factm k)
                                                                       (factm (- n k))))])])
                                       (begin
                                         (set! c-memo (cons (cons (cons n k)
                                                                  c-result)
                                                            c-memo))
                                         c-result)))))])
                comb))

;; test inputs for combm
(define test8 '(0 1))
(define test9 '(0 0))
(define test10 '(6 4))
(define test11 '(1000 500))